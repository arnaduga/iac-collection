# IaC Collection

Personal IaC collections

## AWS-DUP

### Architecture

```mermaid
    graph TD
        %% NETWORK ------------------------------------
        VPC(Default VPC) --> AZ1(Availability Zone 1)
        VPC --> AZ2(Availability Zone 2)
        VPC --> AZ3(Availability Zone 3)

        AZ1 --- sub1
        AZ2 --- sub2
        AZ3 --- sub3


        subgraph APP-db-subnet-group
            sub1(APP_RDS_subnet_1) -- hosts --> RDS
            sub2(APP_RDS_subnet_2) -- hosts --> RDS
            sub3(APP_RDS_subnet_3) -- hosts --> RDS     
        end



        %% Database
        RDS(RDS - APP-database-instance)



        %% Container

        ECR(ECR - app_dev_docker_images)


        EKS(Kubernetes) -- orchestrates --> Fargate(Fargate Compute)
        ECR -- uses --> Fargate

        Fargate -- stores into --> EFS(File Storage)
        Fargate -- reads from --> RDS
```