
## "Copy" image from Docker hub to ECR

```
aws ecr create-repository --repository-name nginx
docker pull nginx:latest
docker tag nginx <1234567890>.dkr.ecr.<region-code>.amazonaws.com/nginx:v1
aws ecr get-login-password --region <region-code> | docker login --username AWS --password-stdin <1234567890>.dkr.ecr.<region-code>.amazonaws.com
docker push <1234567890>.dkr.ecr.<region-code>.amazonaws.com/nginx:v1
```


## VPC Endpoint information

Required VPC endpoints:
- com.amazonaws.eu-west-1.ecr.api
- com.amazonaws.eu-west-1.ecr.dkr
- com.amazonaws.eu-west-1.s3
- com.amazonaws.eu-west-1.logs
- com.amazonaws.eu-west-1.sts
- com.amazonaws.eu-west-1.elasticloadbalancing

```
aws ec2 create-vpc-endpoint --vpc-id <vpc-id> --vpc-endpoint-type Interface --service-name com.amazonaws.eu-west-1.elasticloadbalancing --subnet-id subnet-abababab --security-group-id sg-1a2b3c4d

# ...and the same for all the VPCe to be created

```


## Deploying a private EKS cluster

https://k8s-dev.github.io/posts/eksfargate/
https://docs.aws.amazon.com/eks/latest/userguide/private-clusters.html



## ekstlc and already existing VPC

https://github.com/weaveworks/eksctl/blob/main/examples/02-custom-vpc-cidr-no-nodes.yaml


## Tagging subnets for VPC Subnet discovering

https://aws.amazon.com/premiumsupport/knowledge-center/eks-vpc-subnet-discovery/?nc1=h_ls