#!/bin/bash

# Retrieve the kubectl binary
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

# Retrieve the eksctl binaries
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin

# Retrieve the project yaml file
wget https://gitlab.com/arnaduga/iac-collection/-/raw/main/AWS-DUP/eksctl/config.yaml?inline=false -O config.yaml