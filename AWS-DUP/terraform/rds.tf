
# Database creation
##  Takes: ~18 minutes for creation
##         8 minutes for deletion



resource "aws_db_instance" "default" {
  db_subnet_group_name       = aws_db_subnet_group.default_subnet_group.name
  allocated_storage          = var.rds_mysql_storage
  engine                     = "mysql"
  engine_version             = var.rds_mysql_version
  instance_class             = var.rds_mysql_flavor
  name                       = var.rds_mysql_dbname
  username                   = var.rds_mysql_username
  password                   = var.rds_mysql_password
  parameter_group_name       = "default.mysql5.7"
  skip_final_snapshot        = lower(var.environment) == "prd" ? "false" : "true"
  multi_az                   = true
  storage_encrypted          = true
  identifier                 = "${lower(var.application_trigram)}-database-instance"
  auto_minor_version_upgrade = true
  backup_retention_period    = 35
  deletion_protection        = lower(var.environment) == "prd" ? "true" : "false"
}


output "MySQL" {
  value = aws_db_instance.default.address
}
