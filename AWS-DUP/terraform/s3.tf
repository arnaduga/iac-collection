resource "aws_s3_bucket" "bucket" {
  bucket = "${lower(var.application_trigram)}-${lower(var.environment)}-storage"

  versioning {
    enabled = true
  }

  acl = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
}


output "S3_endpoint" {
  value = aws_s3_bucket.bucket.bucket_domain_name
}
