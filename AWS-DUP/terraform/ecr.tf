resource "aws_ecr_repository" "ecr_default" {

  name = "${lower(var.application_trigram)}_${lower(var.environment)}_docker_images"

  # For prd environment, images are IMMUTABLE.
  image_tag_mutability = lower(var.environment) == "prd" ? "IMMUTABLE" : "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  encryption_configuration {
    encryption_type = "KMS"
  }
}


output "ecr_repo_url" {
  value = aws_ecr_repository.ecr_default.repository_url
}
