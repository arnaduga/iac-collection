terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"

  # Wonderful default tagging feature!
  default_tags {
    tags = {
      environment         = lower(var.environment)
      application         = lower(var.application_name)
      application_trigram = lower(var.application_trigram)
      application_code    = lower(var.application_code)
    }
  }
}
