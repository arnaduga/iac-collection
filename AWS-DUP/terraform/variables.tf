# Common variables for TAGGING ======================================
variable "application_name" {
  type    = string
  default = "MyWonderfulApplication"
}
variable "application_trigram" {
  type        = string
  description = "Trigram of the application"
  default     = "MWA"
}
variable "environment" {
  type    = string
  default = "dev"
  # NOTE: some attributes are defined from this value. For production, please use "prd". Anything else for non production
}
variable "application_code" {
  type    = string
  default = "1234"
}

## Network ==========================================================
variable "rds_subnet_az1_cidr" {
  type        = string
  description = "subnet CIDR range for RDS. Must be part of the VPC range"
  #NOTE: this CIDR is defined as a subset of the development VPC. To be adjusted according to your VPC CIDR
  default = "172.31.128.0/20"
}

variable "rds_subnet_az2_cidr" {
  type        = string
  description = "subnet CIDR range for RDS. Must be part of the VPC range"
  #NOTE: this CIDR is defined as a subset of the development VPC. To be adjusted according to your VPC CIDR
  default = "172.31.144.0/20"
}

variable "rds_subnet_az3_cidr" {
  type        = string
  description = "subnet CIDR range for RDS. Must be part of the VPC range"
  #NOTE: this CIDR is defined as a subset of the development VPC. To be adjusted according to your VPC CIDR
  default = "172.31.160.0/20"
}

# Define RDS variables ==============================================
variable "rds_mysql_version" {
  type        = string
  description = "MySQL version to be used"
  default     = "5.7"
}
variable "rds_mysql_flavor" {
  type        = string
  description = "Instance class size"
  default     = "db.t3.medium"
}
variable "rds_mysql_storage" {
  type        = number
  description = "Amount of storage to be allocated"
  default     = 20
}
variable "rds_mysql_dbname" {
  type        = string
  description = "Name of the DB to be created"
  default     = "wp_database"
}
variable "rds_mysql_username" {
  type      = string
  default   = "wp_db_user"
  sensitive = true
}



resource "random_string" "randomPWD" {
  length  = 24
  special = true
}


variable "rds_mysql_password" {
  type      = string
  sensitive = true
  # DB Password should be set in environment variable BEFORE running the script
  # export TF_VAR_password=(the password)
  default = "tH1sISn0t45tronGPas$wOrd..."
  # DO NOT KEEP THIS PASSWORD default VALUE!
}
