# Prepare the VPC/Subnet for the DB mainly

# Three subnets are created insode the default VPC
# A subnet group is created and groups the 3 subnets
# The DB will use this subnet_group


data "aws_vpc" "selected" {
  default = true
}

## Subnet creation --------------------------------------------------
resource "aws_subnet" "subnet_az1" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = var.rds_subnet_az1_cidr
  availability_zone = "eu-west-1a"
  tags = {
    Name                                 = "${lower(var.application_trigram)}_RDS_subnet_1"
    "kubernetes.io/role/internal-elb"    = 1
    "kubernetes.io/cluster/cluster-name" = "shared"
  }
}

resource "aws_subnet" "subnet_az2" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = var.rds_subnet_az2_cidr
  availability_zone = "eu-west-1b"
  tags = {
    Name                                 = "${lower(var.application_trigram)}_RDS_subnet_2"
    "kubernetes.io/role/internal-elb"    = 1
    "kubernetes.io/cluster/cluster-name" = "shared"
  }
}

resource "aws_subnet" "subnet_az3" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = var.rds_subnet_az3_cidr
  availability_zone = "eu-west-1c"

  tags = {
    Name                                 = "${lower(var.application_trigram)}_RDS_subnet_3"
    "kubernetes.io/role/internal-elb"    = 1
    "kubernetes.io/cluster/cluster-name" = "shared"
  }
}

## Subnet-route association -----------------------------------------

resource "aws_route_table" "eks_route" {

  vpc_id = data.aws_vpc.selected.id

  route = [
  ]

  #route {
  #    cidr_block = "0.0.0.0/0"
  #    gateway_id = aws_internet_gateway.igw.id
  #}

  tags = {
      Name = "team_eks_cluster_route"
  }  
}

resource "aws_route_table_association" "az1" {
  subnet_id      = aws_subnet.subnet_az1.id
  route_table_id = aws_route_table.eks_route.id
}

resource "aws_route_table_association" "az2" {
  subnet_id      = aws_subnet.subnet_az2.id
  route_table_id = aws_route_table.eks_route.id
}

resource "aws_route_table_association" "az3" {
  subnet_id      = aws_subnet.subnet_az3.id
  route_table_id = aws_route_table.eks_route.id
}


## Subnet group creation --------------------------------------------

resource "aws_db_subnet_group" "default_subnet_group" {
  name       = "${lower(var.application_trigram)}-db-subnet-group"
  subnet_ids = [aws_subnet.subnet_az1.id, aws_subnet.subnet_az2.id, aws_subnet.subnet_az3.id]
  tags = {
    Name = "DBsubnetGroup"
  }
}

output "vpc_id" {
  value = data.aws_vpc.selected.id
}

output "subnet_1" {
  value = aws_subnet.subnet_az1.id
}

output "subnet_2" {
  value = aws_subnet.subnet_az2.id
}

output "subnet_3" {
  value = aws_subnet.subnet_az3.id
}

output "aws_db_subnet_group" {
  value = aws_db_subnet_group.default_subnet_group.name
}
