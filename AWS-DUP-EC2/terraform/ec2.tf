# setup launch configuration for the auto-scaling.
resource "aws_launch_configuration" "my_launch_configuration" {

    image_id = "ami-06c01f1a020c06c78"

    instance_type = "t2.micro"
    #key_name = aws_key_pair.my_instance_key_pair.key_name # terraform_learning_key_2
    security_groups = [aws_security_group.my_launch_config_security_group.id]

    #associate_public_ip_address = false
    lifecycle {
        create_before_destroy = true
    }

    #user_data = file("deployment.sh")
}

resource "aws_security_group" "my_launch_config_security_group" {
    vpc_id = data.aws_vpc.selected.id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_autoscaling_group" "my_autoscaling_group" {
    name = "my-autoscaling-group"
    desired_capacity = 2 
    min_size = 2 
    max_size = 3 
    health_check_type = "ELB"

    force_delete = true

    launch_configuration = aws_launch_configuration.my_launch_configuration.id
    vpc_zone_identifier = [
        aws_subnet.private1.id,
        aws_subnet.private2.id 
    ]
    timeouts {
        delete = "15m"
    }
    lifecycle {
        create_before_destroy = true
    }
}

