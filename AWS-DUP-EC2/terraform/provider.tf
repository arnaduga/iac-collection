terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"

  # Wonderful default tagging feature!
  default_tags {
    tags = {
      Environment         = lower(var.environment)
      Application         = lower(var.application_name)
      Application_trigram = lower(var.application_trigram)
      Application_code    = lower(var.application_code)
    }
  }
}
