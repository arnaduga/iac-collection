# Prepare the VPC/Subnet for the DB mainly

# Three subnets are created insode the default VPC
# A subnet group is created and groups the 3 subnets
# The DB will use this subnet_group


data "aws_vpc" "selected" {
  default = true
}

data "aws_availability_zones" "azs" {
}

## Subnet creation --------------------------------------------------

resource "aws_subnet" "private1" {
  vpc_id     = data.aws_vpc.selected.id
  cidr_block = "172.31.100.0/24"

  availability_zone = data.aws_availability_zones.azs.names[0]

  tags = {
    Name = "${lower(var.application_trigram)}-private-1"
  }
}

resource "aws_subnet" "private2" {
  vpc_id     = data.aws_vpc.selected.id
  cidr_block = "172.31.101.0/24"

  availability_zone = data.aws_availability_zones.azs.names[1]

  tags = {
    Name = "${lower(var.application_trigram)}-private-2"
  }
}

## Subnet group creation --------------------------------------------

resource "aws_db_subnet_group" "default_subnet_group" {
  name       = "${lower(var.application_trigram)}-db-subnet-group"
  subnet_ids = [aws_subnet.private1.id, aws_subnet.private2.id]
  tags = {
    Name = "DBsubnetGroup"
  }
}

output "vpc_id" {
  value = data.aws_vpc.selected.id
}

output "subnet_1" {
  value = aws_subnet.private1.id
}

output "subnet_2" {
  value = aws_subnet.private2.id
}

output "aws_db_subnet_group" {
  value = aws_db_subnet_group.default_subnet_group.name
}
