resource "aws_security_group" "my_alb_security_group" {
    vpc_id = data.aws_vpc.selected.id
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_lb" "my_alb" {
    name = "${lower(var.application_trigram)}-alb"
    internal = true
    load_balancer_type = "application"
    security_groups = [aws_security_group.my_alb_security_group.id]
    subnets = [
        aws_subnet.private1.id,
        aws_subnet.private2.id 
    ]
}

resource "aws_lb_listener" "my_alb_listener" {  
    load_balancer_arn = aws_lb.my_alb.arn
    port = 80  
    protocol = "HTTP"
    default_action {    
        target_group_arn = aws_lb_target_group.my_alb_target_group.arn
        type = "forward"  
    }
}

resource "aws_lb_target_group" "my_alb_target_group" {
    port = 80
    protocol = "HTTP"
    vpc_id = data.aws_vpc.selected.id
}

resource "aws_autoscaling_attachment" "my_aws_autoscaling_attachment" {
    alb_target_group_arn = aws_lb_target_group.my_alb_target_group.arn
    autoscaling_group_name = aws_autoscaling_group.my_autoscaling_group.id
}

output "ALB_url" {
    value = aws_lb.my_alb.dns_name
}