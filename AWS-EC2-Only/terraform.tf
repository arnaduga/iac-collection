terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-1"

  # Wonderful default tagging feature!
  default_tags {
    tags = {
      Environment         = "dev"
      Application         = "dup"
      Application_trigram = "dup"
      Application_code    = "dupcode"
    }
  }
}



data "template_file" "initMachine" {
  template = file("template.tpl")
}


resource "aws_instance" "web" {
  ami           = "ami-0d1bf5b68307103c2"
  instance_type = "t3.micro"
  lifecycle {
    create_before_destroy = true
  }
  user_data                   = data.template_file.initMachine.rendered
  associate_public_ip_address = true
  key_name                    = "arnaudG"
  vpc_security_group_ids      = ["sg-093b8a2e6d337eb03"]

}
