#cloud-config

packages:
  - unzip
  - httpd
  - jq
  - php
  - php-cli
  - php-mysql

runcmd:
  - systemctl start httpd
  - systemctl enable httpd
  - echo "<html><body><h1>Hellow world</h1></body></html>" > /var/www/html/index.html
  - echo "<?php phpinfo() ?>" > /var/www/html/php.php
