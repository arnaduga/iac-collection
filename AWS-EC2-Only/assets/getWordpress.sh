#!/bin/sh

version=4.2.4
#version=latest

echo " ----- Downloading the Wordpress version $version"
curl https://wordpress.org/wordpress-$version.tar.gz --silent --output wp-$version.tar.gz

echo " ----- Downloading the expected md5 checksum"
online_md5="$(curl -sL https://wordpress.org/wordpress-$version.tar.gz.md5)"

echo " ----- Calculating the downloaded md5 checksum"
local_md5="$(md5sum wp-$version.tar.gz | cut -d ' ' -f 1)"


if [ "$online_md5" = "$local_md5" ]; then
    echo " ===== CHECKSUM Validated!"
else
    echo " X-X-X CHECKSUM validation error!"
    echo "       For security reason, downloaded file wp-$version.tar.gz will be deleted"
    rm wp-$version.tar.gz
fi